const router = require("express").Router();
const userController = require("../controller/userController");

router.get("/", userController.getUsers);
router.post("/", userController.addUser);

module.exports = router;
