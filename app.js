const express = require("express");
const app = express();
const port = 3000;
const mongoose = require("mongoose");
const userRouter = require("./route/user.route");

require("dotenv").config();

mongoose
  .connect(process.env.MONGODB_CONNECT)
  .then(() => console.log("Connect success"))
  .catch((err) => console.log(err));

app.use("/api/users", userRouter);

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.listen(port, () => console.log(`App listening at http://localhost:3000`));
