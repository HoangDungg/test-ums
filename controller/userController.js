const user = require("../model/user");

async function addUser(req, res) {
  if (!req.body) {
    return res.status(400).json({
      status: "failed",
      msg: "Body is empty.",
    });
  }

  const newUser = new user({
    first_name: req.body.first_name,
    last_name: req.body.last_name,
    email: req.body.email,
    active: req.body.active,
  });
  await newUser
    .save()
    .then((result) => {
      if (!result) {
        return res.status(400).json({
          status: "failed",
          msg: "Add user failed.",
        });
      }
      return res.status(200).json({
        status: "success",
        msg: "Add user success",
        data: newUser,
      });
    })
    .catch((err) => console.log(err));
}

async function getUsers(req, res) {
  user
    .find()
    .then((result) => {
      res.status(200).json({
        status: "success",
        data: result,
      });
    })
    .catch((err) => console.log(err));
}
module.exports = {
  addUser,
  getUsers,
};
