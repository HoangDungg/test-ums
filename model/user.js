const mongoose = require("mongoose");

const userSchema = mongoose.Schema({
  first_name: { type: String },
  last_name: { type: String },
  email: { type: String },
  active: { type: Boolean },
});

module.exports = mongoose.model("sys_user", userSchema);
